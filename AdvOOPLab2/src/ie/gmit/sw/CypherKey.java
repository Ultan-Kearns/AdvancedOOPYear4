package ie.gmit.sw;

public interface CypherKey{
	public void setKey() throws CypherException;
	public String getKey();
}